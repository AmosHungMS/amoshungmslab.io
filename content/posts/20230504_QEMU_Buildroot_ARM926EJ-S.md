---
title: "[嵌入式linux學習] QEMU + Buildroot 模擬 ARM926EJ-S"
date: 2023-05-04T01:25:58+08:00
draft: false
author: "amoshung"
tags: ['qemu', 'buildroot', 'arm', '安裝環境']
categories: ['嵌入式linux學習']
---
# 工具介紹：
## QEMU
QEMU（Quick Emulator）是一個開源的虛擬機器模擬器和虛擬化技術。它允許在一個平台上模擬不同的硬體架構，用於開發、測試和調試跨平台的應用程序。QEMU 的主要特點如下：

**1. CPU 和硬體模擬**：QEMU 支持多種硬體架構，如 x86、x86-64、ARM、MIPS、PowerPC 等。它可以模擬不同的 CPU 類型，並提供對應的硬體模擬，如網絡、串口、USB等。
**2. 跨平台**：QEMU 可以在多種操作系統上運行，如 Linux、Windows、macOS 等。這使開發人員能夠在自己的電腦上模擬和測試針對其他平台的應用程序。
**3. 使用者模式模擬**：QEMU 提供使用者模式模擬，允許在同一個平台上運行編譯為不同架構的二進制程序。例如，在 x86_64 電腦上運行編譯為 ARM 的二進制程序。
**4. 全系統模擬**：QEMU 支持全系統模擬，可以模擬整個計算機系統，包括 CPU、記憶體、硬碟、網絡和其他外設。這使得開發人員可以在虛擬機器上運行完整的操作系統，並在其中開發、測試和調試應用程序。
**5. 與 KVM 整合**：在支持的 Linux 系統上，QEMU 可以與內核虛擬機（KVM）整合，以利用硬體虛擬化技術提高虛擬機器的性能。這使得 QEMU 更適合用於生產環境，提供高性能的虛擬化解決方案。

## Buildroot
Buildroot 是一個開源的嵌入式 Linux 系統構建工具。它使用 Makefile 和 Kconfig 配置系統（與 Linux 內核相似）來自動化整個嵌入式 Linux 系統的構建過程。Buildroot 的主要功能包括：

**1. 生成交叉編譯工具鏈**：Buildroot 可以根據您選擇的目標架構和配置選項生成交叉編譯工具鏈，用於編譯適用於目標平台的應用程序和內核。
**2. 構建 Linux 內核映像**：Buildroot 可以自動從源代碼編譯 Linux 內核，並根據您的配置選項生成內核映像。
**3. 生成根文件系統**：Buildroot 可以生成一個完整的根文件系統（rootfs），包含選定的應用程序、庫和配置文件。您可以選擇所需的應用程序和庫，Buildroot 會自動從源代碼編譯它們並將它們集成到 rootfs 中。
**4. 創建引導加載程序**：Buildroot 還可以編譯和配置引導加載程序（如 U-Boot 或 GRUB），以便在目標硬體上引導內核和根文件系統。
**5. 模擬與調試支持**：Buildroot 內置了對 QEMU 的支持，允許您在模擬器中運行和調試生成的嵌入式 Linux 系統。這對於在真實硬體到達之前進行開發和測試非常有用。

-----

在研究使用qemu的時候可以看到用crosstool-ng跟buildroot兩種工具，在此先釐清一下兩者的差別：

Crosstool-NG 和 Buildroot 都是強大的工具，但它們的目標和使用場景有所不同。 選擇使用哪個工具取決於您的需求和目標。

**Crosstool-NG** 的主要目的是為嵌入式系統生成交叉編譯工具鏈。 它提供了一個配置界面，可以讓您選擇目標架構、C 库、編譯器版本等。 Crosstool-NG 生成的工具鏈可以用於編譯適用於目標系統的程序。 如果您的需求主要集中在編譯適用於特定架構的應用程序，那麼 Crosstool-NG 可能是一個不錯的選擇。

**Buildroot** 是一個用於生成嵌入式 Linux 系統的工具，它可以生成完整的根文件系統（rootfs）、內核映射和引導加載程序。 Buildroot 還包括了許多常見的應用程序和庫，可以很容易地添加到生成的 rootfs 中。 此外，Buildroot 也可以生成交叉編譯工具鏈。 如果您的需求涉及到創建整個嵌入式 Linux 系統（包括工具鏈、根文件系統、內核映射等），那麼 Buildroot 可能更適合您。

總之，如果您的需求主要是生成一個交叉編譯工具鏈，則可以選擇 **Crosstool-NG**。 如果您需要創建一個完整的嵌入式 Linux 系統，包括工具鏈、內核、根文件系統等，則可以選擇 **Buildroot**。 在某些情況下，您甚至可以結合使用這兩個工具，例如使用 Crosstool-NG 生成的工具鏈來編譯 Buildroot 提供的 rootfs。


-----
# QEMU + Buildroot 如何模擬 ARM926EJ-S
## 安裝QEMU
1. 打開終端機：在桌面上按 Ctrl + Alt + T 鍵，或者在應用程序菜單中搜索終端機。
2. 更新系統：在終端機中運行以下命令，以確保系統已更新：
```
sudo apt update
sudo apt upgrade
```
3. 安裝 QEMU：輸入以下命令安裝 QEMU 及其相關套件：
```
sudo apt install qemu qemu-kvm libvirt-bin virtinst bridge-utils cpu-checker
```
4. 驗證安裝：輸入以下命令檢查 QEMU 版本，確保已成功安裝：
```
qemu-system-x86_64 --version
```

## 安裝Buildroot及模擬 ARM926EJ-S
1. 安裝依賴項：在 Ubuntu 上，使用以下命令安裝 Buildroot 的依賴項：
```
sudo apt-get install build-essential libncurses5-dev gawk git subversion libssl-dev gettext unzip zlib1g-dev file python
```
2. 下載 Buildroot：
```
git clone https://github.com/buildroot/buildroot.git
cd buildroot
```
3. 配置 Buildroot：對於 ARM926EJ-S，使用 Versatile Express 配置文件。在 buildroot 目錄中運行：
```
make qemu_arm_versatile_defconfig
```
4. 自定義配置：
```
make menuconfig
```
在彈出的配置界面中，您可以自定義內核、根文件系統、應用程序等。完成自定義後，保存並退出。
5. 編譯 Buildroot：
```
make
```
編譯完成後，將在 output/images 目錄下生成所需的映像文件，例如 zImage（Linux 內核映像）和 rootfs.ext2（根文件系統映像）。
6. 使用 QEMU 模擬 ARM926EJ-S：
```
qemu-system-arm -M versatilepb -cpu arm926ej-s -kernel output/images/zImage -drive file=output/images/rootfs.ext2,if=scsi,format=raw -append "root=/dev/sda console=ttyAMA0,115200" -serial stdio -net nic -net user
```

這將使用生成的內核和根文件系統映像啟動 ARM926EJ-S 模擬器。